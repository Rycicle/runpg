//
//  RPGTableViewCell.h
//  RunPG
//
//  Created by Ryan Salton on 16/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPGTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *cellText;

@end
