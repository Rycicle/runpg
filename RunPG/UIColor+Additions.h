//
//  UIColor+Additions.h
//  RunPG
//
//  Created by Ryan Salton on 15/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Additions)

+ (UIColor *)RPGBackgroundColor;
+ (UIColor *)RPGTextColor;
+ (UIColor *)RPGButtonColor;

@end
