//
//  RPGMenuScene.m
//  RunPG
//
//  Created by Ryan Salton on 15/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "RPGMainScene.h"

@interface RPGMainScene ()

@property (nonatomic, strong) SKNode *foregroundNode;

@property (nonatomic, strong) SKSpriteNode *buttonMenu;
@property (nonatomic, strong) SKSpriteNode *buttonActivity;

@property (nonatomic, strong) SKLabelNode *labelLevel;

@end

@implementation RPGMainScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(3.0, 1.0);
    view.layer.shadowOpacity = 1.0;
    
    self.backgroundColor = [UIColor clearColor];
    
    self.foregroundNode = [SKNode node];
    
    SKSpriteNode *foregroundBG = [SKSpriteNode spriteNodeWithColor:[UIColor RPGBackgroundColor] size:self.size];
    foregroundBG.position = CGPointMake(self.size.width * 0.5, self.size.height * 0.5);
    [self.foregroundNode addChild:foregroundBG];
    
    self.buttonMenu = [SKSpriteNode spriteNodeWithImageNamed:@"buttonMenu"];
    self.buttonMenu.position = CGPointMake((self.buttonMenu.size.width * 0.5) + 20, self.size.height - ((self.buttonMenu.size.height * 0.5) + 20));
    [self.foregroundNode addChild:self.buttonMenu];
    
    self.buttonActivity = [SKSpriteNode spriteNodeWithImageNamed:@"buttonActivity"];
    self.buttonActivity.position = CGPointMake(self.size.width - ((self.buttonActivity.size.width * 0.5) + 20), self.buttonMenu.position.y);
    [self.foregroundNode addChild:self.buttonActivity];
    
    self.labelLevel = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Thin"];
    self.labelLevel.text = @"Level 39";
    self.labelLevel.fontColor = [UIColor RPGTextColor];
    self.labelLevel.position = CGPointMake(self.size.width * 0.5, self.buttonMenu.position.y - (self.labelLevel.fontSize * 0.5) + 5);
    [self.foregroundNode addChild:self.labelLevel];
    
    [self addChild:self.foregroundNode];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        SKNode *node = [self nodeAtPoint:location];
        
        if ([node isEqual:self.buttonMenu])
        {
            
            CGRect menuRect = self.view.frame;
            
            if (self.view.frame.origin.x > 0)
            {
                menuRect = CGRectMake(0, menuRect.origin.y, menuRect.size.width, menuRect.size.height);
            }
            else
            {
                menuRect = CGRectMake(self.size.width - 60, menuRect.origin.y, menuRect.size.width, menuRect.size.height);
            }
            
            [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.view.frame = menuRect;
            } completion:nil];
        }
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
