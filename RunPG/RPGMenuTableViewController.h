//
//  RPGMenuTableViewController.h
//  RunPG
//
//  Created by Ryan Salton on 16/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPGMenuTableViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *menuItemsArray;

@end
