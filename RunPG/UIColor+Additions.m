//
//  UIColor+Additions.m
//  RunPG
//
//  Created by Ryan Salton on 15/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "UIColor+Additions.h"

@implementation UIColor (Additions)

+ (UIColor *)RPGBackgroundColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)RPGTextColor
{
    return [UIColor blackColor];
}

+ (UIColor *)RPGButtonColor
{
    return [UIColor greenColor];
}

@end
