//
//  RPGTableViewCell.m
//  RunPG
//
//  Created by Ryan Salton on 16/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "RPGTableViewCell.h"


@interface RPGTableViewCell ()

@property (nonatomic, strong) UILabel *cellLabel;

@end

@implementation RPGTableViewCell

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        self.cellLabel = [[UILabel alloc] initWithFrame:self.frame];
        self.cellLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:16.0];
        [self.contentView addSubview:self.cellLabel];
    }
    
    return self;
}

- (void)setCellText:(NSString *)cellText
{
    self.cellLabel.text = cellText;
}

@end
