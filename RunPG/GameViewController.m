//
//  GameViewController.m
//  RunPG
//
//  Created by Ryan Salton on 15/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "GameViewController.h"
#import "RPGMainScene.h"
#import "RPGMenuTableViewController.h"

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    RPGMenuTableViewController *menuViewController = [[RPGMenuTableViewController alloc] initWithStyle:UITableViewStylePlain];
    
    [self addChildViewController:menuViewController];
    [self.view addSubview:menuViewController.view];
    
    // Configure the view.
    SKView * skView = [[SKView alloc] initWithFrame:self.view.frame];
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    
    // Create and configure the scene.
    RPGMainScene *scene = [[RPGMainScene alloc] initWithSize:skView.frame.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:scene];
    
    [self.view addSubview:skView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
