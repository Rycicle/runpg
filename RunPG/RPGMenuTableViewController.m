//
//  RPGMenuTableViewController.m
//  RunPG
//
//  Created by Ryan Salton on 16/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "RPGMenuTableViewController.h"
#import "RPGTableViewCell.h"


@implementation RPGMenuTableViewController

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    
    if (self)
    {
        [self.tableView registerClass:[RPGTableViewCell class] forCellReuseIdentifier:@"Cell"];
        
        self.menuItemsArray = [NSMutableArray arrayWithArray:@[@"Customise Character", @"Change Theme", @"Share With Friends", @"Reset"]];
    }
    
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuItemsArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPGTableViewCell *cell = [[RPGTableViewCell alloc] init];
    cell.cellText = self.menuItemsArray[indexPath.row];
    return cell;
}

@end
